export default {
  mode: 'spa',
  /*
  ** Headers of the page
  */
  head: {
    titleTemplate: '%s - ' + process.env.npm_package_name,
    title: process.env.npm_package_name || '',
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: process.env.npm_package_description || ''}
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: {color: '#fff'},
  /*
  ** Global CSS
  */
  css: [],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/firestore-emulator',
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxtjs/vuetify',
    '@nuxtjs/moment',
  ],

  moment: {
    locales: ['fr']
  },
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/pwa',
    'nuxt-fire',
  ],
  /*
  ** vuetify module configuration
  ** https://github.com/nuxt-community/vuetify-module
  */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
  },
  fire: {
    config: {
      apiKey: process.env.FIREBASE_API_KEY_BAN,
      authDomain: "bansi-b2f7f.firebaseapp.com",
      databaseURL: "https://bansi-b2f7f.firebaseio.com",
      projectId: "bansi-b2f7f",
      storageBucket: "bansi-b2f7f.appspot.com",
      messagingSenderId: "601068778157",
      appId: "1:601068778157:web:3cf73b479720f2eaa00c24",
      measurementId: "G-VE8NJ8X2PC"
    },
    services: {
      firestore: true,
      performance: true,
      analytics: true,
    }
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  }
}
