export const state = () => ({
  visitors: [],
})

export const actions = {
  get: async function ({commit, state}, id) {
    const snapshot = await this.$fireStore.collection('users').collection("sites")
    return commit('visitors', {id: snapshot.id, ...snapshot.data()})
  },
}

export const mutations = {
  site: (state, visitors) => state.visitors = {...visitors},
}
