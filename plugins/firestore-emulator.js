export default async (ctx, inject) => {
  if (ctx.isDev) {
    ctx.app.$fireStore.settings({host: "localhost:8200", ssl: false})
  }
}
